/**
 * <p>Snail系统模块</p>
 * <p>系统上下文、配置等。</p>
 * 
 * @author acgist
 * @since 1.0.0
 */
package com.acgist.snail.system;