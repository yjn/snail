/**
 * <p>Snail下载协议模块</p>
 * <p>下载开始前准备：获取下载信息、创建下载任务。</p>
 * <p>对协议进行转换：磁力链接转种子、迅雷下载链接转为真正的下载协议。</p>
 * 
 * @author acgist
 * @since 1.0.0
 */
package com.acgist.snail.protocol;