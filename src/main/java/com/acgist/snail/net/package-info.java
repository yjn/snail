/**
 * <p>Snail网络协议模块，实现下载需要的网络协议。</p>
 * <p>参考文章：https://www.cnblogs.com/LittleHann/p/6180296.html</p>
 * <p>参考文章：https://www.libtorrent.org/udp_tracker_protocol.html</p>
 * <p>参考文章：https://blog.csdn.net/li6322511/article/details/79002753</p>
 * <p>参考文章：https://blog.csdn.net/p312011150/article/details/81478237</p>
 * <p>参考文章：https://wiki.theory.org/index.php/BitTorrentSpecification</p>
 * <p>参考文章：https://blog.csdn.net/weixin_41310209/article/details/87165399</p>
 * <p>常见状态：https://baike.baidu.com/item/uTorrent</p>
 */
package com.acgist.snail.net;